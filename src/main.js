import Vue from 'vue'
import App from './App'
import request from './utils/request'
import uView from "uview-ui";
import towxml from './static/towxml/index'
Vue.use(uView);

Vue.config.productionTip = false

Vue.prototype.request = request
Vue.prototype.towxml = towxml

App.mpType = 'app'

const app = new Vue({
  ...App
})
app.$mount()
