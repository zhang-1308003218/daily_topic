export default (params) => {
  return new Promise((resolve, reject) => {

    const baseUrl = 'https://glowworm.club:8001'
    uni.showLoading({
      title: '加载中...',
    })

    uni.request({
      ...params,
      header:{
        Authorization:getApp().globalData.token || uni.getStorageSync('token')
      }, 
      url: baseUrl + params.url,
      success: (res) => {
        resolve(res.data)
      },
      error: (err) => {
        reject(err)
      },
      complete: () => {
        uni.hideLoading()
      }
    })

  })
}